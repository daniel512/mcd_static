<!DOCTYPE html>
<html lang="pl">
<head>
    <?php include('./includes/head_section.php'); ?>
</head>
<style>
       section {
        background-color: white;
        padding: 100px 15%;
    }
</style>
<body>
   
<?php include('./includes/navbar.php'); ?>
    <section>
        <h1>Regulamin mieszkanieczydom.pl </h1>
        <h2>I Słownik</h2>
        <p>
            O ile w niniejszym Regulaminie występują następujące zwroty, należy rozumieć je następująco:
        </p>
        <ol>
            <li>
                Regulamin – niniejszy regulamin określający prawa i obowiązki korzystania z usług, produktów i serwisów Firmy promowanych znakiem lub/i logo MEDIABRIEF Tomasz Kazior, zwany w dalszej części Regulaminem;             
            </li>
            <li>
                Firma – MEDIABRIEF Tomasz Kazior z siedzibą w Bielsku-Białej 43-316, ul. Giewont 3/27, NIP: 5761486183, REGON: 383773952. 
            </li>
            <li>
                Zasoby – wszystkie oferowane przez Firmę usługi, do których internauta uzyskuje dostęp w ramach prowadzonych przez Firmę witryn internetowych.
                W szczególności pod pojęciem tym rozumie się: fora, czaty, biuletyny, blogi, newslettery, archiwa oraz wszelkie treści i materiały zamieszczone na tych nośnikach;
            </li>
            <li>
                Serwisy – wszystkie prowadzone przez Firmę witryny internetowe;
            </li>
            <li>
                Polityka Prywatności – dokument określający zasady zarządzania danymi osobowymi, stanowiący integralną część Regulaminu;
            </li>
            <li>
                Użytkownik – w niniejszym dokumencie pod pojęciem Użytkownika rozumie się każdą osobę, która nawet 
                w ograniczony sposób skorzystała z narzędzi, produktów lub usług Firmy;
            </li>
            <li>
                Usługa Elektroniczna – wszystkie usługi świadczone drogą elektroniczną, 
                polegające na umożliwieniu Użytkownikom korzystania z Zasobów MEDIABRIEF Tomasz Kazior.
            </li>
        </ol>
        <div id="slider"></div>
        <h2>II Regulamin</h2>
        <p>

        </p>
        <ol>
            <li> 
                Przedmiotem Regulaminu jest określenie zasad prawa i obowiązki Użytkowników oraz relacje pomiędzy Firmą a Użytkownikami. 
            </li>
            <li> 
                Prawa i obowiązki Firmy lub/i Użytkowników określone są wyłącznie w niniejszym Regulaminie, Polityce Prywatności oraz bezwzględnie obowiązujących przepisach prawa.
            </li>
            <li> 
                Każdy z Użytkowników po dokładnym zapoznaniu się z treścią Regulaminu, zobowiązuje się do jego przestrzegania.
            </li>
            <li> 
                Treść niniejszego Regulaminu będzie udostępniona na stronie internetowej www.mieszkanieczydom.pl
            </li>
            <li> 
                Niemożność wyegzekwowania praw wynikających z Regulaminu przez Firmę bądź istnienia praktyk oraz zwyczajów niezgodnych z postanowieniami Regulaminu, 
                nie stanowią podstaw do odstęp od stosowania wszystkich postanowień Regulaminu.
            </li>
            <li> 
                Z Usług Elektronicznych mogą korzystać wszyscy Użytkownicy spełniający określone w Regulaminie wymagania.
            </li>
        </ol>
        <h2>III Ochrona Danych Osobowych oraz Polityka Prywatności</h2>
        <ol>
            <li>
                Polityka Prywatności stanowi integralną i nieodłączną część Regulaminu.
            </li>
            <li>
                Przetwarzanie Danych Użytkownika podanych w procesie Rejestracji oraz Danych udostępnionych w utworzonym profilu przez Użytkownika odbywa się za Zgodą Użytkownika. 
                Zgoda zostaje udzielona w procesie rejestracji przez zaznaczenie odpowiedniego pola typu CheckBox.
            </li>
            <li>
                Administratorem danych osobowych jest MEDIABRIEF Tomasz Kazior z siedzibą w Bielsku-Białej 43-316, ul. Giewont 3/27, NIP: 5761486183, REGON: 383773952.
            </li>
            <li>
                Zgodnie z art. 24 ustawy o ochronie danych osobowych Firma zapewnia wszystkim zarejestrowanym Użytkownikom realizację uprawnień, 
                w szczególności prawo wglądu do własnych danych, prawo żądania aktualizacji i usunięcia danych oraz prawo wniesienia sprzeciwu w przypadkach 
                określonych w przepisach ustawy, korzystając z mechanizmów przekazywania opinii dostępnych przez Zasoby Firmy. Podanie danych jest dobrowolne.
            </li>
            <li>
                Wniesienie sprzeciwu jest równoznaczne z brakiem możliwości dostępu przez Użytkownika do Serwisu Internetowego.
            </li>
            <li>
                Firma nie przekazuje, nie sprzedaje i nie użycza zgromadzonych danych osobowych Użytkowników innym osobom lub instytucjom, chyba, że dzieje 
                się to za wyraźną zgodą lub na życzenie Użytkownika lub też na żądanie uprawnionych na podstawie prawa organów państwa na potrzeby prowadzonych 
                przez nie postępowań lub też w celach, dla których pierwotnie je gromadzono, tylko w sposób dozwolony niniejszym Regulaminem oraz wszelkimi 
                obowiązującymi przepisami prawa. Firma może ujawnić współpracującym podmiotom opracowane zbiorcze oraz ogólne zestawienia statystyczne nie zawierających 
                danych pozwalających na identyfikację poszczególnych Użytkowników.      
            </li>
            <li>
                Usługodawca przetwarza Dane zgodnie z Regulaminem oraz właściwymi przepisami obowiązującymi na terytorium Rzeczypospolitej Polskiej, w tym ustawą 
                z dnia 29 sierpnia 1997 r. o ochronie danych osobowych oraz ustawą z dnia 18 lipca 2002 r. o świadczeniu usług drogą elektroniczną. Przetwarzanie danych 
                następuje jedynie w celu realizacji usług świadczonych przez Serwis oraz w celu wypełnienia zobowiązań wynikających z umów zawartych pomiędzy Serwisem 
                a Użytkownikiem, w celu świadczenia usług drogą elektroniczną oraz innych celów określonych w Regulaminie.   
            </li>
            <li>
            Firma przetwarza dane osobowe Użytkowników głownie w jednym lub w kilku z następujących celów:
                <ul>
                    <li>
                        udostępniania Treści Cyfrowych bez Nośnika,
                    </li>
                    <li>
                        świadczenia w sposób prawidłowy i na najwyższym poziomie usług za pośrednictwem drogi elektronicznej;
                    </li>
                    <li>
                        umożliwienie Użytkownikom, których dane dotyczą dostępu do informacji związanych z Zamówieniami, a także dostępu do zamówionych Treści Cyfrowych bez Nośnika;
                    </li>
                    <li>
                        udzielania odpowiedzi na korespondencję kierowaną do Firmy, w szczególności za pośrednictwem formularza kontaktowego; e. profilowania danych w trakcie zawierania lub wykonywania umowy;
                    </li>
                    <li>
                        przesyłania wiadomości zawierających informacje handlowe lub innych działań marketingowych – za uprzednią wyraźną zgodą Użytkownika.
                    </li>
                    <li>
                    profilowania użytkownika zgodnie z prowadzoną działalnością Firmy w stopniu niezbędnym do realizacji jej statutowych celów oraz obowiązującym prawem.
                    </li>
                </ul>
            </li>
            <li>
                Dane osobowe podane przy okazji wysyłania komentarzy, odpowiedzi na forum, korzystania z czatu, FAQ, porad i wskazówek, przykładów, konkursów, 
                wymiany punktów na nagrody niematerialne, galerii itp. są dostępne dla wszystkich zarejestrowanych Użytkowników korzystających z Zasobów, a ich 
                zakres określa każdy Użytkownik indywidualnie na swoim Koncie. Pomimo licznych procesów weryfikacyjnych Firma nie jest w stanie zabezpieczyć Użytkowników 
                przed osobami prywatnymi lub firmami, które te dane wykorzystają do przesłania nieokreślonych informacji. Dlatego dane te nie podlegają Polityce Prywatności.
            </li>
            <li>
                Wszystkie materiały, informacje i listy przesłane bądź opublikowane w ramach Zasobów Firmy przez Użytkownika bądź też w inny sposób przekazane 
                Firmie są traktowane przez obie strony jako niepoufne i niezastrzeżone. Firma nie ma żadnych zobowiązań względem tych informacji i może swobodnie 
                kopiować, odtwarzać, włączać do innych materiałów i rozpowszechniać bądź wykorzystywać tego typu informacje, według własnego uznania. Informacje 
                o treści pornograficznej, zawierającej przemoc, nieprzyzwoitej, oszczerczej, zniesławiającej lub w jakikolwiek sposób naruszającej prawo nie są dozwolone.           
            </li>
            <li>
                Wszelka korespondencja od Firmy, adresowana bezpośrednio do Użytkowników powinna być traktowana jako poufna. 
                Zabrania się udostępniania takiej korespondencji osobom trzecim bez uprzedniej zgody Firmy.     
            </li>
            <li>
                Firma MEDIABRIEF traktuje ochronę prywatności, w tym ochronę danych osobowych Użytkowników priorytetowo i dokłada wszelkich starań, aby prywatność ta była chroniona. 
                Firma stosuje wszelkie środki ostrożności, aby przekazywane przez Użytkowników dane osobowe były chronione przed ujawnieniem, dostępem osób niepowołanych lub niewłaściwym ich wykorzystaniem.     
            </li>
            <li>
                Wszelkiego rodzaju informacje przechowywane w bazie danych Firmy stanowią jej własność. Wykorzystywanie ich przez Użytkowników byłych i obecnych 
                bez upoważnienia ze strony Firmy jest zabronione i może skutkować usunięciem konta, łącznie z podjęciem działań prawnych, skierowanych przeciwko nim.
            </li>
            <li>
                Przekazanie danych osobowych przez Użytkownika jest dobrowolne, może być jednak niezbędne dla uzyskania dostępu do określonych 
                usług, produktów lub treści cyfrowych bez nośnika lub odpowiedzi na wiadomość.        
            </li>
            <li>
                Niezależnie od danych osobowych Firma gromadzi adresy IP (numery interfejsu sieciowego) oraz identyfikatory urządzeń (anonimowy identyfikator urządzenia) Użytkowników, 
                zapisywane w logach systemowych serwera. Informacje te wykorzystywane są w szczególności w celu zarządzania Zasobami Firmy, rozpoznawania i rozwiązywania problemów 
                związanych z pracą serwerów, analizowania ewentualnych naruszeń bezpieczeństwa, a także w celach statystycznych (anonimowo).       
            </li>
            <li>
                W Zasobach Firmy Użytkownik może znaleźć linki umożliwiające bezpośrednie przejście na inne strony internetowe. Powyższe łącza udostępnia się 
                w charakterze usługi i nie implikują one jakiegokolwiek zatwierdzenia działań lub treści takich witryn ani żadnych związków z ich operatorami. 
                Firma nie odpowiada za politykę ochrony prywatności stosowaną przez właścicieli tych stron. 
            </li>
            <li>
                Firma zachęca Użytkowników do zapoznania się z treścią oświadczeń o ochronie prywatności umieszczonych na stronach gromadzących dane osobowe.
            </li>
        </ol>
        <h2>IV Zasady korzystania z Usług Elektronicznych</h2>
        <ol>
            <li>
                Korzystanie z Usług Elektronicznych, jest równoznaczne z akceptacją przez danego Użytkownika treści niniejszego Regulaminu wraz z zobowiązaniem się tego Użytkownika do jego przestrzegania.   
            </li>
            <li>
                Użytkownicy zobowiązani są do korzystania z Usług Elektronicznych, zgodnie z postanowieniami niniejszego Regulaminu, Polityką Prywatności i obowiązującymi przepisami prawa.
            </li>
            <li>
                Użytkownicy zobowiązani są do powstrzymania się od zamieszczania przy wykorzystaniu Usług Elektronicznych wszelkich treści o charakterze bezprawnym. 
            </li>
            <li>
                Dokonywanie za pośrednictwem Usług Elektronicznych jakichkolwiek działań naruszających powszechnie obowiązujące przepisy prawa jest zabronione. W szczególności, niedozwolone jest:
                <ul>
                   <li>
                       rozpowszechnianie za pośrednictwem Usług Elektronicznych treści naruszających dobra osobiste osób trzecich (w tym innych Użytkowników) lub zagrażających naruszeniem dóbr osobistych;
                    </li> 
                    <li>
                        podejmowanie działań spełniających znamiona przestępstwa lub zachęcających do popełniania przestępstw (w tym zniesławienia lub pomówienia);
                    </li>
                    <li>
                        wzywanie za pośrednictwem Usług Elektronicznych do nienawiści rasowej, wyznaniowej lub etnicznej, czy też propagowanie przemocy i agresji;
                    </li>
                    <li>
                        rozpowszechnianie za pośrednictwem Usług Elektronicznych materiałów naruszających (bądź zagrażających) prawom autorskim i pokrewnym osób trzecich 
                        (w tym zachęcanie do nabycia lub korzystania z nielegalnego oprogramowania);
                    </li>
                    <li>
                        rozpowszechnianie za pośrednictwem Usług Elektronicznych materiałów naruszających lub zagrażających prawom własności przemysłowej 
                        (w tym prawom z rejestracji znaku towarowego lub patentu) lub innym prawom podmiotowym osób trzecich;
                    </li>
                    <li>
                        rozpowszechnianie danych informatycznych zawierających wirusy komputerowe lub inne informatyczne rozwiązania destrukcyjne.     
                    </li>
                </ul>
            </li>
            <li>
                Niedozwolone jest podejmowanie za pośrednictwem Usług Elektronicznych działań sprzecznych z zasadami współżycia społecznego. W szczególności zabronione jest
                rozpowszechnianie za pośrednictwem Usług Elektronicznych treści obraźliwych, obscenicznych lub naruszających powszechnie uznawane zasady etyczne.
            </li>
            <li>
                Usługi Elektroniczne przeznaczone są do rozpowszechniania treści w języku polskim, chyba że dla określenia poszczególnych znaków, elementów, 
                procesów zwyczajowo stosuje się nazwy i określenia obcojęzyczne lub zwyczajowe. Treści publikowane za pośrednictwem Usług Elektronicznych 
                powinny być zredagowane poprawnie, w stopniu umożliwiającym jasny i czytelny przekaz zawartych w nich informacji.
            </li>
            <li>
                Użytkownicy zobligowani są do publikowania w ramach Usług Elektronicznych, wyłącznie treści i materiałów zgodnych z przeznaczeniem oraz tematyką. 
                Zakazuje się publikowania w poszczególnych wątkach dyskusyjnych Usług Elektronicznych treści niezwiązanych z tematem wiodącym.   
            </li>
            <li>
                Publikowanie za pomocą Usług Elektronicznych informacji handlowych w rozumieniu ustawy o świadczeniu usług drogą elektroniczną 
                (w tym reklam, materiałów promocyjnych, komunikatów PR, itp.) oraz innych treści o charakterze komercyjnym, bez uprzedniej pisemnej 
                (pod rygorem nieważności) zgody Firmy jest zabronione.
            </li>
            <li>
                Zakazane jest nadużywanie formatowania przy edytowaniu treści publikowanych przy wykorzystaniu Usług Elektronicznych.
            </li>
            <li>
                Użytkownik chcący uzupełnić lub zmodyfikować treść (wypowiedź) uprzednio rozpowszechnioną przez siebie przy wykorzystaniu Usługi Elektronicznej, zobligowany 
                jest do dokonania tej czynności z wykorzystaniem funkcji “Edytuj” udostępnianej Użytkownikowi.
            </li>
            <li>
                Niedopuszczalne jest publikowanie za pośrednictwem Usług Elektronicznych pytań lub problemów, wraz z żądaniem przesłania odpowiedzi 
                na adres poczty elektronicznej lub innego typu środka indywidualnego porozumiewania się na odległość. Usługi Elektroniczne przeznaczone są do publicznej wymiany informacji i pomocy wszystkim Użytkownikom. 
            </li>
            <li>
                Usługi Elektroniczne przeznaczone są do publikacji i wymiany merytorycznych treści zgodnych z określoną tematyką i przeznaczeniem poszczególnych działów Usług Elektronicznych. 
                Rozpowszechnianie treści odbiegających od określonej tematyki jest zabronione. Zabronione jest również dodawanie do treści publikowanych za pomocą Usług Elektronicznych 
                w ramach danego tematu, kolejnych przekazów (wypowiedzi), nie wnoszących merytorycznie nowych informacji w porównaniu do treści dotychczas opublikowanych w danym wątku. 
                W szczególności zabronione jest: 
                <ul>
                    <li>
                        prowadzenie zbędnych sporów i dyskusji za pośrednictwem Usług Elektronicznych ;
                    </li>
                    <li>
                        rozpowszechnienie za pośrednictwem Usług Elektronicznych wypowiedzi, dotyczących konfliktów między Użytkownikami bądź między Użytkownikami, a Firmą;
                    </li>
                    <li>
                        krytykowanie, wyśmiewanie lub obraźliwe komentowanie wypowiedzi (treści i materiałów) publikowanych za pomocą Usług Elektronicznych przez innych Użytkowników;
                    </li>
                    <li>
                        składanie za pośrednictwem Usług Elektronicznych żądania wyjaśnienia przyczyn: zamknięcia dostępu do treści poprzednio publikowanych 
                        za pomocą Usług Elektronicznych (np. usunięcia wypowiedzi, zamknięcia blogu, zbanowania, wypowiedzenia umowy danemu Użytkownikowi, itp.), 
                        okoliczności dot. sporów, których użytkownikami byli inni Użytkownicy.
                    </li>
                    <li>
                        podszywania się pod Użytkowników administrujących poszczególne Usługi Elektroniczne;
                    </li>
                </ul>
            </li>
            <li>
                Wielkość tzw. “Avatarów”, którymi posługują się Użytkownicy, nie może przekraczać 50 KB. Avatary przekraczające ww., wielkość, będą usuwane z Serwisów.
            </li>
            <li>
                Wielkość tzw. “sygnatury” (tekst umieszczany pod każdą wypowiedzią) (podpis), którą posługuje się Użytkownik, nie może przekraczać 5 linijek tekstu 
                o rozmiarze czcionki nie przekraczającym 10 pkt. Obrazek w sygnaturze nie może przekraczać wysokością 5 linijek tekstu, łącznie z tekstem. Wielkość 
                sygnatury nie może przekraczać 50 KB. Sygnatury niespełniające ww. wymagań będą usuwane z Serwisów. 
            </li>
            <li>
                Firma nie odpowiada za treść wypowiedzi publikowanych za pomocą Usług Elektronicznych, a ograniczenia
                odpowiedzialności dotyczące publikowanych treści określa Regulamin oraz Polityka Prywatności.        
            </li>
            <li>
                Firma może czasowo zawiesić działalność Usług Elektronicznych bez podawania 
                przyczyn takiego działania. Powyższe, nie będzie powodowało powstania roszczeń Użytkowników wobec Firmy. 
            </li>
        </ol>
        <h2>V Administracja</h2>
        <ol>
            <li>
                Zawartość każdej zamieszczonej treści i materiałów, jest wyrażeniem poglądów i opinii autora, a nie Firmy oraz osób wyznaczonych przez nią do 
                administrowania i moderacji Usług Elektronicznych, którzy nie ponoszą za nie odpowiedzialności.
            </li>
            <li>
                Firma (Moderator działający w jej imieniu) ma prawo ingerować w zamieszczane przez użytkowników informacje(tj. zmieniać ich treść), 
                kasować je oraz nadawać użytkownikom tzw. “ostrzeżenia”.
            </li>
            <li>
                Wszelkie zastrzeżenia (reklamacje) dotyczące świadczonych Usług Elektronicznych (w tym zaprzestania ich wykonywania, uniemożliwienia 
                dostępu do niektórych danych, zakłóceń w dostępie do korzystania, itp.) – Użytkownicy mają prawo zgłaszać osobie wyznaczonej przez Firmę 
                do pełnienia funkcji “Moderatora”. Zgłoszenia powyższe mogą być dokonywane za pośrednictwem PW (Prywatnych Wiadomości) lub poczty elektronicznej 
                na adres wybranego Moderatora (w tytule wiadomości należy użyć słowa “REKLAMACJA”). Zgłoszenie reklamacji bezpośrednio za pośrednictwem 
                Usług Elektronicznych nie jest dozwolone i nie będzie rozpatrywane.
            </li>
            <li>
                Moderator jest zobligowany do udzielenia odpowiedzi na Reklamację Użytkownika wraz z uzasadnieniem (decyzja Moderatora),
                w terminie 7 dni od daty jej otrzymania. Odpowiedź na daną Reklamację przesyłana będzie w taki sposób, w jaki przesłano samą Reklamację (czyli: e-mail lub PW).
            </li>
            <li>
                W terminie 7 dni od daty otrzymania odpowiedzi (decyzji) Moderatora, Użytkownik ma prawo do złożenia odwołania od decyzji Moderatora, 
                do osoby wyznaczonej przez Firmę, do pełnienia funkcji Administratora Usługi Elektronicznej. Odwołanie może być złożone za pośrednictwem 
                poczty elektronicznej na adres kontakt@mieszkanieczydom.pl (w tytule wiadomości należy użyć słowa “ODWOŁANIE”) Zgłoszenie odwołania za 
                pośrednictwem Usługi Elektronicznej, nie jest dozwolone i nie będzie rozpatrywane.
            </li>
            <li>
                W razie bezskutecznego upływu terminu na złożenie odwołania do Administratora – poczytuje się, że Użytkownik zaakceptował sposób załatwienia sprawy określony w decyzji Moderatora.
            </li>
            <li>
                Administrator jest zobligowany do udzielenia odpowiedzi na odwołanie Użytkownika wraz z uzasadnieniem (decyzja Administratora), 
                w terminie 7 dni od daty jego otrzymania. Odpowiedź na dane odwołanie przesyłana będzie w taki sposób, w jaki je przesłano (czyli: e-mail lub PW). 
            </li>
            <li>
                Odpowiedź Administratora na odwołanie od decyzji Moderatora kończy postępowanie reklamacyjne prowadzone przez Firmę.
            </li>
        </ol>
        <h2> VI Usunięcie lub ograniczenie dostępu</h2>
        <ol>
            <li>
                Zgodnie z Regulaminem oraz Polityką Prywatności każdy z Użytkowników ma prawo do określenia części zakresu udostępnienia danych osobowych, 
                modyfikacji tych danych oraz zakresu korzystania z Usług Elektronicznych. Dokładne regulacje określają Regulamin oraz Polityka Prywatności.
            </li>
            <li>
                W przypadku niewłaściwego korzystania z Usług Elektronicznych Firma i działający w jej imieniu Administrator/Moderator mogą wysłać „Ostrzeżenia” do Użytkownika.
            </li>
            <li>
                “Ostrzeżenie” nie jest metodą nacisku, a jedynie informacją o błędnym postępowaniu w rozumieniu Firmy i osób administrujących.
                Ostrzeżeń jest pięć, po których – w przypadku drastycznego naruszenia zasad regulaminu – Firma (Administrator działający w jej imieniu) 
                ma możliwość usunięcia Użytkownikowi dostępu do Usług Elektronicznych.
            </li>
            <li>
                O zablokowaniu dostępu do Usług Elektronicznych decyduje Firma (Administrator działający w jej imieniu), po ewentualnej konsultacji z Użytkownikami opiekującymi się danymi Usługami Elektronicznymi.
            </li>        
            <li>
                Po zablokowaniu dostępu do Usług Elektronicznych Użytkownikowi przysługuje okres 3 dni roboczych na wyjaśnienie zaistniałej sytuacji. 
                Przekroczenie tego terminu powoduje trwałe usunięcie możliwości korzystania z Usług Elektronicznych. Decyzja Firmy (Administratora działającego w jej imieniu) 
                jest nieodwołalna, a Użytkownik bezwarunkowo się z nią zgadza, nie roszcząc żadnych praw.
            </li>
        </ol>
        <h2> VII Postanowienia końcowe</h2>
        <ol>
            <li>
                Firma zastrzega dla siebie wyłączne prawo do zmian lub/i modyfikowania Regulaminu, które bezwzględnie obowiązują wszystkich Użytkowników. 
                Wszelkie zmiany wchodzą w życie w momencie ich opublikowania na stronie internetowej: www.mieszkanieczydom.pl
            </li>
            <li>
                Zmiany wprowadzone do Regulaminu zaczynają obowiązywać po upływie 10 dni roboczych od ukazania się na stronie internetowej www.mieszkanieczydom.pl. 
                Użytkownicy zobowiązani są do zapoznania się z nową wersją Regulaminu, przed dniem jego wejścia w życie oraz stosowania postanowień w, nim zawartych. 
                Nowy regulamin zastępuje wcześniejszą wersję Regulaminu.      
            </li>
            <li>
                Wszelkie spory wynikłe z tytułu wykonania zobowiązań związanych w Regulaminie będą rozstrzygane przez sąd właściwy dla siedziby Firmy lub Sąd przez nią wybrany.
            </li>
        </ol>
    </section>
    <?php include('./includes/footer.php'); ?>

</body>
</html>