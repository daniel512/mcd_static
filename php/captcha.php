<?php
// Checks if form has been submitted
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    function post_captcha($user_response) {
        $fields_string = '';
        $fields = array(
            'secret' => '6Lc5RbsZAAAAAOmSJkvfEKvK5ofRGTRliwRl1pZQ',
            'response' => $user_response
        );
        foreach($fields as $key=>$value)
        $fields_string .= $key . '=' . $value . '&';
        $fields_string = rtrim($fields_string, '&');

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://www.google.com/recaptcha/api/siteverify');
        curl_setopt($ch, CURLOPT_POST, count($fields));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, True);

        $result = curl_exec($ch);
        curl_close($ch);

        return json_decode($result, true);
    }

    // Call the function post_captcha
    $res = post_captcha($_POST['g-recaptcha-response']);

    if (!$res['success']) {
        // What happens when the CAPTCHA wasn't checked
        header("Location: https://mieszkanieczydom.pl/error.php");
    } else {
        // If CAPTCHA is successfully completed...
        require_once("./send_mail.php");
        $email = $_POST['email'];
        $phone = $_POST['phone'];
        $name = $_POST['name'];    
        $message = $_POST['message'];
        $agreement = $_POST['agreement'];
        if(is_null($agreement))
        {
            header("Location: https://mieszkanieczydom.pl/error.php");
        } 
        else 
        {
            send_mail();
        }       
    }
} 