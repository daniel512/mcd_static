<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

require '../vendor/phpmailer/phpmailer/src/Exception.php';
require '../vendor/phpmailer/phpmailer/src/PHPMailer.php';
require '../vendor/phpmailer/phpmailer/src/SMTP.php';

require '../vendor/autoload.php';



function send_mail() 
{
    $title   = 'Wiadomość z MCD';
    $date    = date("Y/m/d");
    $name    = $_POST['name'];
    $email   = $_POST['email'];
    $message = $_POST['message'];
    $to      = 'kontakt@mieszkanieczydom.pl';
    
    $body = 
    "
        <span style='font-size: 22px; font-family: calibri; font-weight: bold'> 
        Wiadomość z serwisuj mieszkanieczydom.pl z dnia $date
        </span>
        <br><span style='font-size: 16px; font-family: calibri;'>Imię: $name </span>
        <br><span style='font-size: 16px; font-family: calibri;'>E-mail: $email </span>
        <br><span style='font-size: 16px; font-family: calibri;'>treść: $message </span>
        <br> Zgoda zaznaczona: 
        <span style='font-size: 11px color: grey;'> Pana/Pani dane uzupełnione w formularzu kontaktowym zostaną przetworzone w celu udzielenia odpowiedzi na zadane pytanie, 
        w kwestii zawarcia umowy lub realizacjii bieżącej umowy. Podstawą przetwarzania Pana/Pani danych jest art. 6 ust. 1 lit. b) 
        RODO. Administratorem Twoich danych osobowych jest MEDIABRIEF Tomasz Kazior ul. Giewont 3/27, Bielsko-Biała 43-316, 
        NIP: 5761486183, REGON: 383773952. Przysługuje Państwu prawo do dostępu do swoich danych, ich sprostowania, usunięcia lub 
        ograniczenia przetwarzania, jak również prawo do wniesienia sprzeciwu wobec przetwarzania oraz prawo do przenoszenia 
        danych. Więcej informacji znajdziesz tutaj: Polityka Prywatności </span>
    ";

    $mail = new PHPMailer(true); // create a new object
    $mail->IsSMTP(); // enable SMTP
    $mail->SMTPDebug = 0; // debugging: 1 = errors and messages, 2 = messages only
    $mail->SMTPAuth = true; // authentication enabled
    $mail->SMTPSecure = 'tls'; // secure transfer enabled REQUIRED for Gmail
    $mail->Host = "smtp.gmail.com";
    $mail->Port = 587; // or 587
    $mail->CharSet = 'UTF-8';
    $mail->IsHTML(true);
    $mail->Username = "mieszkanieczydom.mb@gmail.com";
    $mail->Password = "########";
    $mail->SetFrom("mieszkanieczydom.mb@gmail.com");
    $mail->Subject = $title;
    $mail->Body = $body;
    
    $mail->AddAddress($to);

    if(!$mail->Send()) {
        echo "Mailer Error: " . $mail->ErrorInfo;
    } else {
        header("Location: https://mieszkanieczydom.pl/dziekujemy.php");
    }
}


