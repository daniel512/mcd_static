<!DOCTYPE html>
<html lang="pl">
<head>
    <?php include('../includes/head_section_2.php'); ?>
</head>

<style>
    table {
        border-collapse: collapse;
    }

    table, tr, th, td {
        border: 1px solid black;
    }

    th, td {
        width: 20%;
        min-width: 100px;
        padding: 2% 3%;
    }

    th {
        background-color: #EEEEEE;
        font-weight: bold;
    }
</style>
<?php include('../includes/navbar.php'); ?>
<?php include('../includes/slider.php'); ?>
<div class="post-container">
    <div class="post">
        <div class="back-button">
            <a href="https://www.mieszkanieczydom.pl/">
                Powrót        
            </a>
        </div>
        <h1>       Mieszkanie czy dom?</h1>
        <span class="date"> 12.06.2020</span>
        <div class="post-image" style="background-image: url('https://www.mieszkanieczydom.pl/public/images/img1.jpeg')"></div>
        <p>
        Twoja pierwsza odpowiedź na to pytanie może niestety opierać się na uprzedzeniach, stereotypach i błędnych wyobrażeniach. 
        Jeżeli myślisz, że dom poza miastem jest idealnym rozwiązaniem, to zastanów się nad tym, że będziesz musiał codziennie kilka 
        lub więcej kilometrów dojeżdżać do pracy i stać w korku. Natomiast, gdy marzysz o mieszkaniu w bloku, to pomyśl o dzieciach, 
        które hałasują na górze. Jak widzisz nawet najpiękniejsze marzenie, gdy już je spełnisz, może mieć swoje wady. Nie znaczy to 
        oczywiście, że chcemy Cię zniechęcić do kupowania domu, czy mieszkania. Chcemy Ci tylko pokazać, jak to realnie wygląda i na 
        co powinieneś być przygotowany. Zanim zaciągniesz kredyt hipoteczny lub wypłacisz z banku oszczędności życia, to radzimy Ci 
        na chłodno przeanalizować wszystkie za i przeciw. W tym wpisie omówimy najpopularniejsze mity dotyczące mieszkania w bloku 
        oraz w domu za miastem. Wspomnimy również to kosztach zakupu i utrzymania obu tych nieruchomości. Mamy nadzieję, że ten artykuł 
        pomoże Ci w podjęciu dobrej decyzji. 
        </p>
        <h2>Jak wygląda życie w podmiejskim domu?</h2>
        <p>
        Pewnie z pełnym przekonaniem odpowiesz, że życie na wsi to sielanka. Można sobie wyjść na zewnątrz, zrobić grilla, 
        a dzieci i pies mają się gdzie wybiegać. W dodatku na wsi panuje to, czego w mieście Ci tak bardzo brakuje, czyli cisza i spokój.
         Snując te piękne wizje, zapominasz o jednej rzeczy. Jeżeli dom jest Twój, to musisz codziennie o niego dbać, bo nie będziesz na wakacjach 
         i nikt za Ciebie nie skosi trawnika, nie rozpali w piecu, ani nie wystawi kosza ze śmieciami. Jesienią będzie Cię czekało sprzątanie liści, 
         a zimą odśnieżanie. W dodatku sąsiedzi mogą być tak samo uciążliwi, jak w mieście. Mogą nie pilnować swoich psów i kosić trawnik o 6 rano.  
        </p>
        <p>
        A co z wymarzoną przestrzenią dla Ciebie i dzieci? Tutaj jest już trochę lepiej. Będziesz mógł mieć swój ogródek, do którego 
        będziesz mógł zapraszać znajomych i rodzinę. Dzieci będą miały miejsce do zabawy. Możesz zrobić dla nich piaskownicę lub basen. 
        Jednak tutaj też niestety jest pewien haczyk. Mianowicie, gdy masz duży dom, to oczywiście masz również dużo przestrzeni, 
        którą możesz się nacieszyć, ale jednocześnie jest to związane z większą odpowiedzialnością. Mamy tu na myśli to, że musisz 
        się przygotować na to, że często będziesz się zmagał z różnymi usterkami. Jeżeli sam potrafisz naprawić różne rzeczy, 
        to jesteś na wygranej pozycji, bo zaoszczędzisz na fachowcach. Jednak z drugiej strony, będziesz musiał znaleźć czas, 
        żeby zreperować popsuty kran, a to nie jest takie proste. 
        </p>
        <p>
        Kolejnym mitem na temat mieszkania w podmiejskim domu jest to, że na wsi jest zdrowe i czyste powietrze. 
        Ile jest w tym prawdy? Wszystko zależy od tego, w jakiej części Polski znajduje się Twój wymarzony dom. 
        Oczywiście smog będzie mniejszy niż na przykład w Warszawie, ale gdy trafisz na nieodpowiedzialnych sąsiadów, 
        którzy palą w piecu dosłownie wszystko, co wpadnie im w ręce, to raczej nie będziesz mógł się nacieszyć tym tak zachwalanym przez agentów nieruchomości świeżym powietrzem. 
        </p>
        <h2>A jak na tym tle wypada życie w mieszkaniu?</h2>
        <p>
        Chyba najczęściej powtarzanym przekonaniem, jeżeli chodzi o życie w bloku, jest to, że tam nigdy nie ma ciszy i spokoju. 
        To prawda, że gdy masz wokół siebie wiele osób i w dodatku blok jest położony blisko ulicy, to trudno jest o całkowicie wyeliminować hałas. 
        Jednak nie oznacza to, że sąsiedzi są głośno od rana do nocy i w ogóle nie można odpocząć. Wiadomo, że większość ludzi też pracuje i też ceni sobie ciszę,
         spokój i nie będzie hałasować, tylko po to, żeby zrobić Ci na złość.     
        </p>
        <p>
        Kolejnym często powtarzanym stereotypem jest to, że mieszkanie w bloku jest bezpieczne. Tutaj wszystko zależy od tego, w jakiej dzielnicy się znajdziesz. 
        Każdy wie o tym, że są strefy mniej i bardziej bezpieczne. Chociaż monitoring jest coraz częściej montowany, to nadal nie jest on czymś powszechnym. 
        Włamania i kradzieże mogą się zdarzyć wszędzie. Jednak prawdą jest, że zabezpieczenie mieszkania przed tego rodzaju zdarzeniami, jest o wiele prostsze 
        i tańsze niż podmiejskiego domu. Potencjalny włamywacz może się dostać do Twojego domu tylko przez drzwi wejściowe oraz balkon, jeżeli go posiadasz. 
        W praktyce oznacza to, że żebyś mógł się czuć bezpiecznie, wystarczą Ci porządne drzwi antywłamaniowe oraz dobre ubezpieczenie. Tak na wszelki wypadek. 
        </p>
        <p>
        Następnym często powtarzanym stwierdzeniem jest to, że gdy mieszkasz w mieszkaniu, to masz dużo czasu dla siebie, bo odpada Ci 
        konieczność koszenia trawy i rozpalania w piecu w zimę. Tak, mieszkanie pod tym względem zdecydowanie wygrywa. Zajmowanie się nim 
        zajmuje o wiele mniej czasu, a nawet, gdy coś się zepsuje o wiele szybciej możesz znaleźć odpowiedniego fachowca.           
        </p>
        <p>
        Czy mieszkanie w bloku jest droższe w utrzymaniu niż podmiejski dom na wsi? Dokładnie się przyjrzymy jeszcze w dalszej części tego artykułu, 
        ale teraz chcielibyśmy tylko zaznaczyć, że dom, gdy przeliczymy koszty utrzymania na metr powierzchni użytkowej okazuje się tańszy. Tak dla nas też jest to pewne zaskoczenie.         
        </p>
        <h2>Co jest bardziej opłacalne dom czy mieszkanie?</h2>
        <p>
        Wszystko zależy od tego, gdzie zlokalizowana jest konkretna nieruchomość oraz od tego, jaką ma powierzchnię. 
        Tak ogólnie można przyjąć, że 1 m2 powierzchni użytkowej domu jest mniej kosztowny od takiej samej powierzchni 
        mieszkania. Średnia różnica pomiędzy ceną 1 m2 mieszkania w Warszawie, a domu w podmiejskiej okolicy to trochę 
        ponad 4000 zł. Jak widzisz, różnica jest ogromna, na korzyść domów. Jednak trzeba pamiętać, że są to domy położone 
        pod miastem, a więc dochodzą tutaj koszty codziennych dojazdów do pracy.         
        </p>
        <h2>
        Jakie są koszty utrzymania domu i mieszkania?       
        </h2>
        <p>
        Koszty utrzymania domu i mieszkania będą zależne od wielu zmiennych. W przypadku domu jednorodzinnego masz przecież do wyboru różnego rodzaju ogrzewanie, 
        na przykład piec na ekogroszek lub piec gazowy, a właśnie ocieplenie domu pochłania największą część domowego budżetu. W przypadku zakupu mieszkania 
        będziesz się musiał liczyć z koniecznością comiesięcznego opłacania czynszu, który będzie różny w zależności od metrażu, dzielnicy i wieku bloku. 
        Do tego dochodzi też konieczność naprawy różnych usterek, ubezpieczenia, opłaty za wodę, prąd, ubezpieczenia. Jeżeli chodzi o koszty utrzymania domu 
        i mieszkania, to dom jest tańszy, ale wynika to tylko i wyłącznie z tego, że te opłaty można podzielić przez większą ilość metrów kwadratowych.        
        </p>

    </div>
</div>
    <?php include('../includes/footer.php'); ?>

</body>
</html>