<!DOCTYPE html>
<html lang="pl">
<head>
    <?php include('../includes/head_section_2.php'); ?>
</head>

<style>
    table {
        border-collapse: collapse;
    }

    table, tr, th, td {
        border: 1px solid black;
    }

    th, td {
        width: 20%;
        min-width: 100px;
        padding: 2% 3%;
    }

    th {
        background-color: #EEEEEE;
        font-weight: bold;
    }

    span {
        font-size: 12px; 
    }
</style>
<?php include('../includes/navbar.php'); ?>
<?php include('../includes/slider.php'); ?>
<div class="post-container">
    <div class="post">
        <div class="back-button">
            <a href="https://www.mieszkanieczydom.pl/">
                Powrót        
            </a>
        </div>
        <h1>       Mieszkaniowy zawrót głowy. Czy warto kupować nieruchomości w czasie pandemii?</h1>
        <span class="date">  07.07.2020</span>
        <div class="post-image" style="background-image: url('https://www.mieszkanieczydom.pl/public/images/img2.jpeg')"></div>
        <p>
            Rynek nieruchomości w czasach sprzed wszechobecnego dziś COVID-19 miał się świetnie. 
            Pomimo wysokich cen, nie brakowało dotąd chętnych na zakup mieszkania. Klienci nabywali je dla siebie, 
            bądź celem wynajęcia i zarobku. Agenci nieruchomości z kolei mieli ręce pełne roboty.
        </p>
        <h2>Rzut okiem na rok 2019</h2>
        <p>
                    Z analizy wykonanej przez Narodowy Bank Polski na temat sytuacji na rynku nieruchomości w IV kwartale 2019 roku, można zebrać istotne wnioski:
        <br>1.	Ceny nieruchomości mieszkaniowych nadal rosły – zarówno na rynku pierwotnym, jak i wtórnym (przy wzroście kosztów budowy, jako wynik wysokiego popytu w gospodarce).
        <br>2.	Rosły średnie ceny ofertowe i transakcyjne za metr kwadratowy, nie tylko mieszkań w stanie deweloperskim na rynkach pierwotnych, ale i na rynkach wtórnych.
        <br>3.	Wzrosła roczna sprzedaż mieszkań i liczba mieszkań gotowych do zamieszkania. Wskaźnik czasu sprzedaży mieszkań na rynku pierwotnym pokazał zaś szybką sprzedaż kolejnych ofert i mniejsze niż dotąd możliwości wyboru mieszkań do kupna.
        4.	Zaobserwowano nienadążanie podaży za popytem, przez co produkcja mieszkań w toku pozostawała na relatywnie niskim poziomie.

        </p>
        <p>
            Statystyki zatem potwierdzają, że obszar obrotu nieruchomościami pozostawał jednym ze stabilniejszych rynków w Polsce, 
            z tendencją wzrostową i cen i sprzedaży. Według danych GUS wartość za metr kwadratowy
             mieszkania rośnie nieprzerwanie od 2013 roku, a obecnie sięga nawet ponad 10 000 złotych (np. w Warszawie).
        </p>

        <h2>Rozwój pandemii, a ceny mieszkań</h2>
        <p>
            Specjaliści zasadniczo podkreślają, że każdy kryzys gospodarczy, prędzej czy później, może odbić się też na rynku nieruchomości. 
            Pandemia i związane z nią restrykcje spowodowały z początku niewielkie ograniczenie liczby transakcji na rynku pierwotnym i wtórnym. 
            Można też śmiało stwierdzić, że ceny za metr kwadratowy mieszkań utrzymywały się na podobnym poziomie, 
            a nawet rosły względem cen z końca 2019 roku. Wspomnianą sytuację obrazują poniższe dane.
        </p>
        <h2>Średnie ceny ofertowe za metr kwadratowy mieszkań dla 7* największych miast w Polsce</h2>
        <div class="table-wrapper">
            <table>
                <tr>
                    <th>Terminarz</th>
                    <th>Rynek pierwotny</th>
                    <th>Rynek wtórny</th>
                </tr>
                <tr>
                    <td><b>IV kwartał 2019</b></td>
                    <td>8 779,00 zł</td>
                    <td>9 072,00 zł</td>
                </tr>
                <tr>
                    <td><b>I kwartał 2020</b></td>
                    <td>9 144,00 zł</td>
                    <td>9 473,00 zł</td>
                </tr> 
            </table>
            <span>
            <i> Źródło: Baza cen nieruchomości mieszkaniowych BaRN Narodowego Banku Polskiego </i> <br>
            * Gdańsk, Gdynia, Kraków, Łódź, Poznań, Warszawa, Wrocław
            </span>
        </div>
      
      
        <h2>Średnie ceny transakcyjne za metr kwadratowy mieszkań dla 7* największych miast w Polsce</h2>
        <div class="table-wrapper">
            <table>
                <tr>
                    <th>Terminarz</th>
                    <th>Rynek pierwotny</th>
                    <th>Rynek wtórny</th>
                </tr>
                <tr>
                    <td><b>IV kwartał 2019</b></td>
                    <td>8 223,00 zł</td>
                    <td>7 702,00 zł</td>
                </tr>
                <tr>
                    <td><b>I kwartał 2020</b></td>
                    <td>8 506,00 zł</td>
                    <td>7 919,00 zł</td>
                </tr>
            </table>
            <span>
                <i> Źródło: Baza cen nieruchomości mieszkaniowych BaRN Narodowego Banku Polskiego </i> <br>
                * Gdańsk, Gdynia, Kraków, Łódź, Poznań, Warszawa, Wrocław
            </span>
        </div>
         <p>
            Dane pokazują zatem, że rynek nieruchomości poradził sobie doskonale z pandemią. Przedsiębiorstwa zajmujące się obrotem nieruchomościami, 
            które nie przetrwały tego okresu, prawdopodobnie jeszcze przed falą wirusa źle funkcjonowały. Winny mógł być ich wewnętrzny system zarządzania. 
            Co prawda, na początku pandemii pojawiła się fala obaw o całkowity zastój gospodarki, cała Europa niemal stanęła w miejscu na jakiś czas. 
            Mimo to klienci nie przestali kupować mieszkań.
         </p>
         <h2>Jak się kupuje mieszkania w czasie pandemii?</h2>
         <p>
            Sprzedawcy nieruchomości mają wiele sposobów na to, by nie stracić klientów, z jednoczesnym zachowaniem standardów sanitarnych. 
            Przede wszystkim oferty można dokładnie przejrzeć nie ruszając się z domu, przykładowo za pomocą licznych zdjęć czy wirtualnej wizyty w wybranym mieszkaniu. 
            Stosuje się również wideorozmowy. Prezentacja mieszkań odbywa się też z zachowaniem dystansu i z minimalną liczbą oglądających. 
            Zakupy na rynku nieruchomości mogą zatem pozostawać całkowicie bezpieczne. Mieszkania początkowo sprzedawały się nieco wolniej, 
            między innymi ze względu na ograniczone funkcjonowanie urzędów, banków. Teraz jednak wszelkie instytucje wracają do pracy na „pełnych obrotach”.
         </p>
         <h2>Czy warto zatem kupować mieszkania w dobie pandemii?</h2>
         <p>
            Z ekonomicznego punktu widzenia, tak samo warto, jak i w miesiącach przed atakiem COVID-19, a może nawet bardziej. 
            Ceny ofertowe bowiem od początku bieżącego roku utrzymywały się na podobnym poziomie, a nawet stopniowo rosły względem końca 2019 roku. 
            Niektórzy specjaliści szacują obecnie, że wzrost wartości mieszkań będzie teraz mocno spowolniony. Możliwy jest nawet kilkumiesięczny zastój cen, 
            celem obserwacji dokąd zmierza rynek nieruchomości oraz jakie będą oficjalne statystyki dla drugiego kwartału 2020 roku, jeśli chodzi o zakup mieszkań.
         </p>
         <p>
            Warto też pamiętać, że ceny ofertowe nie muszą się równać faktycznym cenom transakcyjnym. Normą w branży nieruchomości są negocjacje co do wartości wybranego mieszkania. Właściciele takich lokali bardzo często godzą się na pewne obniżki cenowe, choćby minimalne.
            Koszty zakupu mieszkań prawdopodobnie niebawem powrócą do swojego dotychczasowego trendu wzrostowego, ponieważ popyt będzie nadal bardzo wysoki. 
            Przede wszystkim wielu klientów uważa taki zakup za najlepszą lokatę dla swoich finansów – przynoszącą też konkretne zyski. Dla wielu inwestycje 
            w nieruchomości pozostają najbezpieczniejszą i rentowną formą ulokowania majątku i osiągania korzyści finansowych.    
         </p>
         <p>
            Generalnie spodziewać się jeszcze można w miarę stabilnych cen, choć mogą one wzrastać razem z całkowitym otwarciem gospodarki już końcem maja. 
            Jeżeli planowaliśmy zakup mieszkania, warto to zrobić teraz. Być może cena wzrośnie tuż przed jesienią. Rosnąć może też cena domów, ponieważ 
            zamknięci w czterech ścianach Polacy poczuli potrzebę posiadania swojego własnego zielonego zakątka, by móc spędzać czas chociaż we własnym ogródku.          
         </p>
    </div>
</div>
    <?php include('../includes/footer.php'); ?>

</body>
</html>