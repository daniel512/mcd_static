export class Slider {


    constructor(slider, numOfSlides, imagesCollection)
    {
        this.currentSlide     = 0;
        this.slider           = slider;
        this.numOfSlides      = numOfSlides;
        this.imagesCollection = imagesCollection;
    }

    nextSlide()
    {
        console.log(this.currentSlide);
        if(this.currentSlide < this.numOfSlides -1)
        {    
            ++this.currentSlide;
            this.slider.children[0].innerText = this.imagesCollection[this.currentSlide].text;
            this.slider.style.backgroundImage = `url(${this.imagesCollection[this.currentSlide].image})`;
        }
        else 
        {
            this.currentSlide = 0;
            this.slider.children[0].innerText = this.imagesCollection[this.currentSlide].text;
            this.slider.style.backgroundImage = `url(${this.imagesCollection[this.currentSlide].image})`;
        }     
    }

    previousSlide()
    {
        if(this.currentSlide > 0)
        {
            --this.currentSlide;
            this.slider.children[0].innerText = this.imagesCollection[this.currentSlide].text;
            this.slider.style.backgroundImage = `url(${this.imagesCollection[this.currentSlide].image})`;
        }
        else 
        {
            this.currentSlide = this.numOfSlides - 1;
            this.slider.children[0].innerText = this.imagesCollection[this.currentSlide].text;
            this.slider.style.backgroundImage = `url(${this.imagesCollection[this.currentSlide].image})`;
        }     
    }


}