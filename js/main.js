const d              = document;
const F              = new Functions();
let navBar           = d.getElementById('navBar');
let burger           = d.getElementById('burger')
let triggerElement   = d.getElementById('slider');
let sliderElement    = d.getElementById('slider');
let sliderButtonLeft = d.getElementById('sliderButtonLeft');
let sliderButtonRight= d.getElementById('sliderButtonRight');

let sliderImages = 
[
    {image: 'https://www.mieszkanieczydom.pl/public/images/bg.jpeg', text: ''},
    {image: 'https://www.mieszkanieczydom.pl/public/images/img4.jpeg', text: ''},
    {image: 'https://www.mieszkanieczydom.pl/public/images/img5.jpeg', text: ''}
];


let slider = new Slider(sliderElement, 3, sliderImages);
let sliderInterval = setInterval(function() {slider.nextSlide()}, 7000);

try 
{
    sliderButtonLeft.onclick = () => 
    {
        clearInterval(sliderInterval);
        slider.previousSlide();   
    }

    sliderButtonRight.onclick = () => 
    {
        clearInterval(sliderInterval);
        slider.nextSlide();  
    }
} 
catch (e) {}

window.addEventListener("scroll", () => {
    styleNavBar(navBar, triggerElement);   
})

let nav = new MyNavbar(navBar, burger);