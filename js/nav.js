export let styleNavBar = (navElement, triggerElement) => 
{
    let rect = navElement.getBoundingClientRect();
    let triggerPoint = triggerElement.getBoundingClientRect();
    
    if(rect.bottom > (triggerPoint.bottom - 200)) 
    {
        navElement.classList.add("scrolled");
    }
    else 
    {
        navElement.classList.remove("scrolled");
    }
}

export class MyNavbar {
    constructor(domElement, activator) 
    {
        this.element   = domElement;
        this.activator = activator;
        activator.onclick = () => 
        {
            this.handleClick();
        }
    }

    handleClick()
    {
        if( !this.element.classList.contains('active') )
        {
            this.element.classList.add("active");         
        }     
        else
        {
            this.element.classList.remove("active");
        }        
    }
}