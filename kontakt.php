<!DOCTYPE html>
<html lang="pl">
<head>
    <?php include('./includes/head_section.php'); ?>
    <script src='https://www.google.com/recaptcha/api.js'></script>
</head>
<body>
<?php include('./includes/navbar.php'); ?>
<div id="slider"></div>
<div class="container">
    <div class="text-n-form-wrapper">
        <div class="text">
            <p>
                Będzie nam niezmiernie miło, jeśli zechcesz współpracować przy tworzeniu naszej bazy wiedzy na temat nieruchomości. 
            </p>
            <p>
                Jesteśmy otwarci na każdą współpracę. Nie ma znaczenia czy jesteś deweloperem i chcesz, aby nasz portal posłużył Ci jako 
                narzędzie do promowania nieruchomości, czy osobą związaną z rynkiem nieruchomości, która pragnie podzielić się swoją wiedzą lub doświadczeniem w tym zakresie. 

                Jeśli czujesz, że dzięki Tobie nasz portal może rozbudować się o nowe, przydatne informacje lub ciekawe oferty nieruchomości – bardzo chętnie porozmawiamy z Tobą na ten temat. 
                Być może prowadzisz biznes, który może iść w parze z naszą działalnością, wnosząc na rynek nieruchomości dowolną wartość dodaną 
                – z zaciekawieniem poznamy Twoją wizję współpracy i rozważymy jej podjęcie.
            </p>
            <p>
                Zapraszamy :)
            </p>
     
        </div>

        <div class="form-container">
        
            <div class="form-wrapper">
                <form method="POST" action="./php/captcha.php">
                <h2>Napisz do nas</h2>
                    <input required name="name" type="text" placeholder="Imię / Firma" />
                    <input required name="email" type="email" placeholder="E-mail" />
                    <textarea required name="message" rows="4" placeholder="W czym możemy pomóc?"></textarea>
                    <label for="agreement">
                        <input required id="agreement" name="agreement" type="checkbox" />
                        <span> 
                            Pana/Pani dane uzupełnione w formularzu kontaktowym zostaną przetworzone w celu udzielenia odpowiedzi na zadane pytanie, 
                            w kwestii zawarcia umowy lub realizacjii bieżącej umowy. Podstawą przetwarzania Pana/Pani danych jest art. 6 ust. 1 lit. b) 
                            RODO. Administratorem Twoich danych osobowych jest MEDIABRIEF Tomasz Kazior ul. Giewont 3/27, Bielsko-Biała 43-316, 
                            NIP: 5761486183, REGON: 383773952. Przysługuje Państwu prawo do dostępu do swoich danych, ich sprostowania, usunięcia lub 
                            ograniczenia przetwarzania, jak również prawo do wniesienia sprzeciwu wobec przetwarzania oraz prawo do przenoszenia 
                            danych. Więcej informacji znajdziesz tutaj: <a href="./polityka.php" target="_blank"><br>Polityka prywatności </a>
                        </span>
                    </label>
                    <div class="g-recaptcha" data-sitekey="6Lc5RbsZAAAAAMIfNvR3eO_UzzcklxUVsZJB7zVe"></div>
                    <input type="submit" value="Wyślij" />
                </form>
            </div>
        </div>
    </div>

</div>

    
    
<?php include('./includes/contact_footer.php'); ?>
</html>