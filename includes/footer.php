<footer>
        <div class="logo-wrapper">
            <a href="https://mieszkanieczydom.pl/">
                <img src="https://www.mieszkanieczydom.pl/public/images/logo.png" alt="MieszkanieCzyDom" />
            </a>

            <div class="links">
                <a href="https://mieszkanieczydom.pl/regulamin.php" target="_blank">Regulamin</a>
                <a href="https://mieszkanieczydom.pl/polityka.php" target="_blank">Polityka&nbsp;prywatności</a>
            </div>
        </div>
        <div class="about">
            <p>
                Chcesz wiedzieć więcej na temat rynku nieruchomości? Poszukujesz inspiracji do urządzenia nowego mieszkania, zastanawiasz się, w jakie nieruchomości warto zainwestować pieniądze – na naszej stronie internetowej znajdziesz wiele przydatnych artykułów na każdy temat, związany z nieruchomościami. 
                Jeśli nie znajdziesz w nich odpowiedzi na nurtujące Cię pytanie – <a href="https://mieszkanieczydom.pl/kontakt.php"><b>napisz&nbsp;do&nbsp;nas</b></a>, a być może stanie się ono tematem naszego kolejnego artykułu.
            </p>
        </div>
  
      
    </footer>
    <div class="copy">
    &copy; 2020 All right reserved MEDIABRIEF
    </div>