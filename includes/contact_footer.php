<div class="contact-footer">
    <footer>
        <div class="logo-wrapper">
            <a href="https://mieszkanieczydom.pl/">
                <img src="https://www.mieszkanieczydom.pl/public/images/logo.png" alt="MieszkanieCzyDom" />
            </a>

            <div class="links">
                <a href="https://mieszkanieczydom.pl/regulamin.php" target="_blank">Regulamin</a>
                <a href="https://mieszkanieczydom.pl/polityka.php" target="_blank">Polityka&nbsp;prywatności</a>
            </div>
        </div>
        <div class="about">
            <p>
                Chcesz wiedzieć więcej na temat rynku nieruchomości? Poszukujesz inspiracji do urządzenia nowego mieszkania, zastanawiasz się, w jakie nieruchomości warto zainwestować pieniądze – na naszej stronie internetowej znajdziesz wiele przydatnych artykułów na każdy temat, związany z nieruchomościami. 
                Jeśli nie znajdziesz w nich odpowiedzi na nurtujące Cię pytanie – <a href="https://mieszkanieczydom.pl/kontakt.php"><b>napisz&nbsp;do&nbsp;nas</b></a>, a być może stanie się ono tematem naszego kolejnego artykułu.
            </p>
        </div>
        <div class="address">

            <p>
                
            <a href="mailto:kontakt@mieszkanieczydom.pl">kontakt@mieszkanieczydom.pl</a> <br>
            +48 731 205 040 <br><br>
            MEDIABRIEF Tomasz Kazior <br>
            ul. Giewont 3/27 <br>
            Bielsko-Biała 43-316 <br> <br>
            

            NIP: 5761486183 <br>
            REGON: 383773952
            </p>
        </div>
      
    </footer>
</div>

    <div class="copy">
    &copy; 2020 All right reserved MEDIABRIEF
    </div>