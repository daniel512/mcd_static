    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Mieszkanie czy Dom</title>
    <link rel="stylesheet" href="../public/css/style.css">
    <script src="../public/js/main.js" type="module"></script>
    <link rel="preload" href="https://www.mieszkanieczydom.pl/public/images/bg.jpeg" as="image">
    <link rel="preload" href="https://www.mieszkanieczydom.pl/public/images/img4.jpeg" as="image">
    <link rel="preload" href="https://www.mieszkanieczydom.pl/public/images/img5.jpeg" as="image">
