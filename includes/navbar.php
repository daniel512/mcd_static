<nav id="navBar">
        <div id="burger" class="burger">
            <div class="item item-1"></div>
            <div class="item item-2"></div>
            <div class="item item-3"></div>
        </div>
        <div class="logo-wrapper">
            <a href="https://mieszkanieczydom.pl/">
                <img src="https://mieszkanieczydom.pl/public/images/logo.png" alt="MieszkanieCzyDom"/>
            </a> 
        </div>
        <ul>
            <li>
                <a href="https://mieszkanieczydom.pl/">Aktualności</a>
            </li>
            <li>
                <a href="https://mieszkanieczydom.pl/kontakt.php">Kontakt</a>
            </li>
        </ul>
</nav>