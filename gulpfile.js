const { src, dest } = require('gulp');
const babel = require('gulp-babel');
const rename = require('gulp-rename');
const uglify = require('gulp-uglifyes');
const concat = require('gulp-concat');
const sass = require('gulp-sass');
const cssConcat = require('gulp-concat-css');
const cssMinify = require('gulp-minify-css');
const imagemin = require('gulp-imagemin');

function css() {
    return src('css/*.scss')
    .pipe(sass())
    .pipe(cssConcat('style.css'))
   // .pipe(cssMinify('style.css'))
    .pipe(dest('public/css/'));
}

function js() {
  return src(['js/functions.js', 'js/nav.js', 'js/slider.js', 'js/main.js'])
    .pipe(concat('concat.js'))
    .pipe(rename('main.js'))
    .pipe(uglify())
    .pipe(dest('public/js/'));
}

function images() {
    return src('assets/*')
        .pipe(imagemin())
        .pipe(dest('public/images/'))
}

exports.js = js;
exports.css = css;
exports.images = images;