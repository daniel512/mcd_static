<!DOCTYPE html>
<html lang="pl">
<head>
    <?php include('./includes/head_section.php'); ?>
</head>
<body>
<?php include('./includes/navbar.php'); ?>
<?php include('./includes/slider.php'); ?>
    <section class="blog blog-l">
        <h2>Aktualności</h2>
        <div class="recent">
            <div class="first-post">
            <a href="./posts/1.php">
                <div class="post-image">
                    <div class="description">
                        <div class="title">
                       
                                Mieszkaniowy zawrót głowy. Czy warto kupować nieruchomości w czasie pandemii?
                            
                        </div>
                        <div class="date">
                            07.07.2020
                        </div>
                    </div>
                </div>
                </a>
            </div> 
            <div class="post-card ">
                <a href="./posts/2.php">
                    <div class="post-image"></div>
                </a>
                <div class="description">
                    <div class="title">
                        <a href="./posts/2.php">
                        Mieszkanie czy dom?
                        </a> <br>
                        <span class="date"> 12.06.2020 </span>
                    </div>
                    <div class="text"> 
                                Twoja pierwsza odpowiedź na to pytanie może niestety opierać się na uprzedzeniach, stereotypach i&nbsp;błędnych wyobrażeniach. 
                    Jeżeli myślisz, że dom poza miastem jest idealnym rozwiązaniem, to zastanów się nad tym, że będziesz musiał codziennie kilka 
                    lub więcej kilometrów dojeżdżać do pracy i&nbsp;stać w korku. Natomiast, gdy marzysz o&nbsp;mieszkaniu w&nbsp;bloku, to pomyśl o&nbsp;dzieciach, 
                    które hałasują na górze. Jak widzisz nawet najpiękniejsze marzenie, gdy już je spełnisz, może mieć swoje wady. Nie znaczy to 
                    oczywiście, że chcemy Cię zniechęcić do kupowania domu   
                    </div>
                </div>
                <div class="shutter"></div>
            </div>
            <div class="post-card ">
                <a href="./posts/3.php">
                    <div class="post-image"></div>
                </a>
                <div class="description">
                    <div class="title">
                        <a href="./posts/3.php">
                            Flipping nieruchomości
                        </a> <br>
                        <span class="date"> 15.05.2020 </span>
                    </div>
                    <div class="text">
                    Kupić tanio, sprzedać drogo – ten cel przyświeca wszystkim osobom zajmującym się handlem, w tym handlem nieruchomościami. Ale co zrobić, aby 
                    w krótkim czasie znacząco zwiększyć wartość zakupionej nieruchomości? W artykule tłumaczymy czym jest flipping nieruchomości i jak w łatwy sposób osiągać zyski ze sprzedaży mieszkań lub domów.
                    </div>
                </div>
                <div class="shutter"></div>
            </div>

        </div>
        
    </section>
    <?php include('./includes/footer.php'); ?>

</body>
</html>